## Please note that this repository is deprecated. Please visit [drip repository](https://gitlab.com/bloodyhealth/drip) for the current repo that gets updated but also feel free to check out this application written in NativeScript.


# Bloody Health Cycle Tracker

A menstrual cycle tracking app that's open-source and leaves your data on your phone. Use it to track your menstrual cycle or for fertility awareness!

## Development setup

1. Install NativeScript and Android Studio according to the instructions [here](https://docs.nativescript.org/start/quick-setup)

2. Start a virtual device in Android Studio (or make sure it's already running, you should see a phone on your screen)

3. Clone this repository:

    ```
    git clone git@gitlab.com:bloodyhealth/bleed.git
    cd bleed
    ```

4. Run `npm install`

5. ... and `npm start` !

6. NativeScript will watch and build your code changes and you should see them running immediately in Android Studio.

7. We recommend installing an [ESLint plugin in your editor](https://eslint.org/docs/user-guide/integrations#editors). There's a `.eslintrc` file in this project which will be used by the plugin to check your code for style errors and potential bugs. 

## Tests
You can run the tests with `npm test`.
