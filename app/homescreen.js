const frames = require("ui/frame")
const moment = require("moment")

function goToInputSelect() {
  const navigationOptions = {
    moduleName: "input-select",
    context: {
      date: moment(Date.now())
    }
  }
  frames.topmost().navigate(navigationOptions)
}

function goToCalendarView() {
  frames.topmost().navigate("calendar-view")
}

module.exports = {
  goToInputSelect,
  goToCalendarView
}
