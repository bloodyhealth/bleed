const state = require('./state')

function init(args) {
  const page = args.object
  const temperatureList = page.getViewById("temp-list")
  temperatureList.bindingContext = { temperatures: state.temperatures }
}

module.exports = {
  init
}