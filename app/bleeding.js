const state = require('./state')
const bleedingCategories = ["none", "spotting", "light", "medium", "heavy"]

let page
let bindings

function onNavigatingTo(args) {
  page = args.object
  bindings = {
    date : page.navigationContext.date.format('MMM Do YY'),
    sbSelectedIndex: 0,
    exclude: false
  }
  page.bindingContext = bindings
}

function saveBleeding() {
  state.bleedings.unshift({
    value: bleedingCategories[bindings.sbSelectedIndex],
    exclude: bindings.exclude
  })
  page.frame.navigate("bleeding-list")
}

module.exports = {
  onNavigatingTo,
  saveBleeding,
}
