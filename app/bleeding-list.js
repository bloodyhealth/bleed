const state = require('./state')

function init(args) {
  const page = args.object
  const bleedingList = page.getViewById("bleeding-list")

  const bleedingsWithExcludeLabel = state.bleedings.map(bleedingObject => {
    const newBleedingItem = {
      value: bleedingObject.value
    }
    if (bleedingObject.exclude) {
      newBleedingItem.value = `${newBleedingItem.value} (excluded)`
    }
    return newBleedingItem
  })

  bleedingList.bindingContext = { bleedings: bleedingsWithExcludeLabel}
}

module.exports = {
  init
}