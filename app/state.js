const ObservableArray = require("tns-core-modules/data/observable-array").ObservableArray

const state = {
  temperatures: new ObservableArray([]),
  bleedings: new ObservableArray([])
}

module.exports = state