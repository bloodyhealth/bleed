const frames = require("ui/frame")
let navigationContext

function onNavigatingTo(args) {
  const page = args.object
  navigationContext = page.navigationContext
  page.bindingContext = {
    date : navigationContext.date.format('MMM Do YY')
  }
}

function goToTempInput() {
  frames.topmost().navigate({
    moduleName: "temp-input",
    context: navigationContext
  })
}

function goToBleedingInput() {
  frames.topmost().navigate({
    moduleName: "bleeding",
    context: navigationContext
  })
}

module.exports = {
  onNavigatingTo,
  goToTempInput,
  goToBleedingInput
}