const frames = require("ui/frame")
const moment = require("moment")

const bindings = {}

let page

function init(args) {
  page = args.object
  bindings.date = new Date(),
  page.bindingContext = bindings
}

function goToInputSelect() {
  const navigationOptions = {
    moduleName: "input-select",
    context: {
      date: moment(bindings.date)
    }
  }
  frames.topmost().navigate(navigationOptions)
}

module.exports = {
  init,
  goToInputSelect
}
