const state = require('./state')

const bindings = {
  temperature: null,
}

let page

function init(args) {
  page = args.object
  bindings.date = page.navigationContext.date.format('MMM Do YY')
  page.bindingContext = bindings
}

function saveTemperature() {
  if (bindings.currentValue === null) return
  state.temperatures.unshift({ value: bindings.currentValue })
  bindings.currentValue = Math.floor(bindings.currentValue)
  page.frame.navigate("temp-list")
}


module.exports = {
  init,
  saveTemperature
}